@extends('layouts.base')
@section('title')Evento @stop
@section('content')
    <section class="hero-area bg_img" data-background="{{URL::asset('images/page-header.jpg')}}">
        <div class="container">
            <h1 class="title m-0">Eventos</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Eventos
                </li>
            </ul>
        </div>
    </div>
    <section class="blog-section padding-bottom padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-single">
                        <div class="post-item details-post">
                            <div class="post-thumb">
                                <img src="{{asset('storage/'.$event->image)}}" alt="blog">
                                <div class="post-date">
                                    <span>{{$event->created_at->format('d')}}</span>
                                    <span>{{$event->created_at->format('M')}}</span>
                                </div>
                            </div>
                            <div class="post-content">
                                <h4 class="title">
                                    {{$event->title}}
                                </h4>
                                <p>{!! $event->description !!}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>







@stop
