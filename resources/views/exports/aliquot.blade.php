<table>
    <thead>
    <tr>

        <th style="background: yellow">Usuario</th>
        <th style="background: yellow">Valor</th>
        <th style="background: yellow">Local</th>
        <th style="background: yellow">Fecha de Pago</th>
        <th style="background: yellow">Mes a Pagar</th>
        <th style="background: yellow">Año</th>
    </tr>
    </thead>
    <tbody>
    @foreach($aliquots as $aliquot)
        <tr>

            <td>{{$aliquot->name}}</td>
            <td>{{$aliquot->payment_value}}</td>
            <td>{{$aliquot->number_shop}}</td>
            <td>{{$aliquot->payment_date}}</td>
            <td>{{$aliquot->mes}}</td>
            <td>{{$aliquot->year}}</td>


        </tr>
    @endforeach
    </tbody>
</table>
