@extends('layouts.base')
@section('title'){{$shop->name}} @stop
@section('content')
    <section class="hero-area bg_img" data-background="{{URL::asset('images/page-header.jpg')}}">
        <div class="container">
            <h1 class="title m-0">Blog</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Inicio</a>
                </li>
                <li>
                    Tiendas
                </li>
            </ul>
        </div>
    </div>
    <section class="blog-section padding-bottom padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-single">
                        <div class="post-item details-post">
                            <div class="post-thumb">
                                <img src="{{asset('storage/'. $shop->image)}}" alt="blog">

                            </div>
                            <div class="post-content">
                                <h4 class="title">
                                    {{$shop->name}}
                                    @if(isset($shop->facebook_link))<br><a href="{{$shop->facebook_link}}" style="margin-top: 5px; font-size: 15px"><i class="fab fa-facebook-f"></i></a>@endif
                                    <h5><strong>Número del Local: {{$shop->number_shop}}</strong></h5>
                                </h4>
                                <p> {!! $shop->description !!}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


@stop
