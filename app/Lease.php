<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Lease extends Model
{
    public function owner(){
        return $this->belongsTo(Shop::class, 'local_id');
    }
}
