<?php

namespace App\Http\Controllers;

use App\DepartentProduct;
use App\Department;
use App\Shop;
use App\ShopProduct;
use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\DB;


class ProductsController extends Controller
{
    public function index(){
        $types = Type::with('products')->get();
        $products = Product::with('categories')->get();
        return view('products/index', ['types'=>$types, 'products'=>$products]);
    }

    public function searchShop($slug){
        $product = Product::where('slug',$slug)->first();
        $shops = Shop::all();
        $shopsProducts = DB::table('shop_product')->get();
        return view('products/search_shop', ['product'=>$product, 'shops'=>$shops, 'shopsProducts'=>$shopsProducts]);

    }
}
