@extends('voyager::master')
@section('page_title','Descargar Reportes')

@section('page_header')
    <h2 class="page-title">Reportes De Alícuotas</h2>
    @include('voyager::multilingual.language-selector')
@stop
@section('content')

    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body" >
                        {!! Form::open(['route'=>'voyager.aliquot.download','method'=>'POST']) !!}
                        <div class="row">
                            @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif
                                <div class="form-group col-md-2">
                                    <label for="local_name">Número del local</label>
                                    {!! Form::text('local', null,['style'=>'margin-top:5px', 'class'=>'form-control', 'placeholder'=>'Local','required'=>'true']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="date_start">Fecha Inicio</label>
                                    {!! Form::date('date_start', null,['style'=>'margin-top:5px', 'class'=>'form-control', 'placeholder'=>'Fecha Inicio','required'=>'true']) !!}
                                </div>
                                <div class="form-group col-md-2" style="margin-bottom: 300px">
                                    <label for="date_end">Fecha Final</label>
                                    {!! Form::date('date_end', null,['style'=>'margin-top:5px', 'class'=>'form-control', 'placeholder'=>'Fecha Final','required'=>'true']) !!}
                                </div>
                            <div class="form-group col-md-2">
                                {!! Form::submit('Descargar Reporte', ['class' =>'btn btn-primary btn-block btn-md pull-right']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}

                    </div>

                </div>

            </div>

        </div>

    </div>



@stop
