<?php

namespace App\Http\Controllers;

use App\Export;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;


class ReportAliquotUser extends Controller
{
    public function __construct()
    {
        set_time_limit(0);
        ini_set('max_execution_time',3000);
        ini_set('memory_limit','256M');
    }
    public function index(){
        return view('report_aliquot.form');
    }
    public function export(){
        session_start();
        $_SESSION["local"] = \request('local');
        $_SESSION["date_start"] = \request('date_start');
        $_SESSION["date_end"] = \request('date_end');
        $name_document = "reporte-alicuota-";
        $model = $name_document;
        $exportable = "App\\Exports\\Aliquot";

        return Excel::download(new $exportable, "{$model}.xlsx")  ;
    }



}
