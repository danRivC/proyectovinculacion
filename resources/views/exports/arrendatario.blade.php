<h1>Arriendos</h1>
<table>
    <thead>
    <tr>
        <th style="background: yellow">Usuario</th>
        <th style="background: yellow">Valor</th>
        <th style="background: yellow">Local</th>
        <th style="background: yellow">Fecha de Pago</th>
        <th style="background: yellow">Mes a Pagar</th>
        <th style="background: yellow">Año</th>
    </tr>
    </thead>
    <tbody>
    @foreach( $arrendatarios as  $arrendatario )
        <tr>

            <td>{{$arrendatario->name}}</td>
            <td>{{$arrendatario->payment_value}}</td>
            <td>{{$arrendatario->number_shop}}</td>
            <td>{{$arrendatario->payment_date}}</td>
            <td>{{$arrendatario->mes}}</td>
            <td>{{$arrendatario->year}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
