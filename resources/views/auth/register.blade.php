@extends('layouts.base')

@section('content')

    <section class="hero-area bg_img" data-background="images/page-header.jpg">
        <div class="container">
            <h1 class="title m-0">Iniciar Sesión</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Iniciar Sesión
                </li>
            </ul>
        </div>
    </div>

    <section class="account-section padding-top padding-bottom bg_img bg_xxl_contain bg_right_bottom"
             data-background="images/account/account-bg.png">
        <div class="container">
            <div class="account-wrapper">
                <div class="account-area">
                    <h3 class="account-title">Crea una nueva cuenta</h3>
                    <form class="sign-up-form"  method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <input placeholder="Nombre" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror


                        </div>
                        <div class="form-group">
                            <input placeholder="Apellido" id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('lastname') }}" required autocomplete="last_name" autofocus>
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input placeholder="Número de teléfono" id="phone_number" type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>

                            @error('phone_number')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input placeholder="Contraseña" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            <span class="view-pass" id="view-pass-02">
                                <i class="flaticon-eye"></i>
                            </span>
                            @error('password')
                            
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input  placeholder="Repite la Contraseña " id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            <span class="view-pass" id="view-pass-03">
                                <i class="flaticon-eye"></i>
                            </span>
                        </div>



                        <div class="form-group w-100">
                            <input type="submit" value="Registrar">
                        </div>
                    </form>
                    <div class="forget-pass-group text-center m-0 mt-md-3">
                        <p>Ya tienes una cuenta? <a href="{{url('login')}}">Inicia Sesión</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>






@endsection
