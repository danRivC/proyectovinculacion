@extends('layouts.base')
@section('title')Blog-Chiriyacu @stop
@section('content')
    <section class="hero-area bg_img" data-background="{{URL::asset('images/page-header.jpg')}}">
        <div class="container">
            <h1 class="title m-0">Blog</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Blog
                </li>
            </ul>
        </div>
    </div>
    <section class="blog-section padding-bottom padding-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-single">
                        <div class="post-item details-post">
                            <div class="post-thumb">
                                <img src="{{asset('storage/'.$post->image)}}" alt="blog">
                                <div class="post-date">
                                    <span>{{$post->created_at->format('d')}}</span>
                                    <span>{{$post->created_at->format('M')}}</span>
                                </div>
                            </div>
                            <div class="post-content">
                                <h4 class="title">
                                    {{$post->title}}
                                </h4>
                                <p>{!! $post->body !!}</p>


                                @if(count($comments)>0)
                                    <div class="meta-post">
                                        <div class="left">
                                            <div class="comment">
                                                <a href="#0">{{$comments->count()}} Comentarios:</a>
                                            </div>
                                            <div class="tags">
                                                Autor: {{$post->author->name}}
                                            </div>
                                        </div>

                                    </div>
                                    <h5 class="comment-number"><strong></strong></h5>
                                    <div class="comments">
                                        <h3 class="title">{{$comments->count()}} Comentarios</h3>
                                        <ul class="vos-comments">
                                            @foreach($comments as $comment)
                                                <hr>
                                                <div class="comment-item" style="margin-top: 35px">
                                                    <div class="comment-thumb">
                                                        <a href="#0">
                                                            <img src="{{asset('storage/'.$comment->author->avatar)}}"
                                                                 alt="blog">
                                                        </a>
                                                    </div>
                                                    <div class="comment-content">
                                                        <h6 class="sub-title">
                                                            <a href="#0">{{$comment->author->name}}</a>
                                                        </h6>
                                                        <div class="d-flex justify-content-between meta">
                                                            <a href="#0" class="date">{{$comment->created_at}}</a>
                                                        </div>
                                                        <p>{{$comment->content}}</p>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </ul>
                                    </div>
                                @else
                                    <div class="meta-post">
                                        <div class="left">
                                            <div class="comment">
                                                <a href="#0">No hay Comentarios:</a>
                                            </div>
                                            <div class="tags">
                                                Autor: {{$post->author->name}}
                                            </div>

                                            <div class="comments">
                                                <h3 class="title">No hay Comentarios</h3>
                                            </div>
                                        </div>

                                    </div>
                                @endif

                            </div>
                        </div>
                        <div class="comments">
                            @if (Auth::guest())
                                <h3 class="title">Para comentar inicia sesión</h3> <a href="{{route('login')}}">aquí</a>
                            @else
                                <h3 class="title">Deja un comentario</h3>

                                <form class="blog-comment-form" method="post" action="{{route('comment.save')}}">
                                    @csrf
                                    <input type="hidden" value="{{$post->id}}" name="post_id">
                                    <input type="hidden" value="{{$post->slug}}" name="post_slug">

                                    <div class="form-group">
                                        <textarea placeholder="Escribe tu comentario" name="content"></textarea>
                                        @if($errors->has('content'))
                                            <span role="alert" class="alert alert-danger">
                                        <strong>{{$errors->first('content')}}</strong>@endif
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Post Your Comment">
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>



@stop
