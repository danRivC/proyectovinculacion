<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Aliquot extends Model
{
    public function owner(){
        return $this->belongsTo(Shop::class, 'shop_id');
    }
}
