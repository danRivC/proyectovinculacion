@extends('layouts.base')
@section('title')Tiendas @stop
@section('content')

    <section class="hero-area bg_img" data-background="images/page-header.jpg">
        <div class="container">
            <h1 class="title m-0">Tiendas que venden: <br>{{$product->name}}</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Tiendas
                </li>
            </ul>
        </div>
    </div>
    <section class="blog-section padding-bottom padding-top">
        <div class="container">
            <div class="row mb-30-none justify-content-center">
                @foreach($shops as $shop)
                    @foreach($shopsProducts as $shopProduct)
                        @if($shopProduct->shop_id ==$shop->id and $shopProduct->product_id == $product->id )
                    <div class="col-md-6 col-lg-4">
                        <div class="post-item">
                            <div class="post-thumb">
                                <a href="{{url('shops', ['id'=>$shop->id])}}">
                                    <img src="{{asset('storage/'.$shop->image)}}" alt="blog">
                                </a>

                            </div>
                            <div class="post-content">
                                <h4 class="title">
                                    <a href="{{url('shops', ['id'=>$shop->id])}}">{{$shop->name}}</a>
                                </h4>
                                <p>Numero de local: {{$shop->number_local}}</p>
                                <a class="read-more" href="{{url('shops', ['id'=>$shop->id])}}">Leer más<i
                                        class="fas fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>

                        @endif
                        @endforeach
                @endforeach
            </div>
        </div>
        <div class="blog-pagination text-center">

        </div>

        </div>
    </section>


@stop
