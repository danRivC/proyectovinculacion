@extends('layouts.base')
@section('title', 'Centro Comercial Chiriyacu')
@section('content')
    <section class="banner-slider text-center wow fadeIn">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="banner-section bg-none bg_img" data-background="images/banner/ba2.jpg">
                    <div class="container">
                        <div class="banner-content">
                            <span class="category">
                                Visita el centro comercial en donde si existe el !! AHORRO !!
                            </span>
                            <h1 class="title">Los mejores precios todos los días </h1>
                            <p>Obten los mejores beneficios en cualquier local</p>
                            <div class="video-button-group">
                                <a href="#0" class="custom-button active">Conócenos</a>
                                <div class="v-button">
                                    <!-- <span>watch video</span> -->
                                    <a data-rel="lightcase:myCollection" href="https://www.youtube.com/watch?v=2aVmfYSL7qQ" class="video-button"><i
                                            class="flaticon-play-button"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide">
                <div class="banner-section bg-none bg_img" data-background="images/banner/ba3.jpg">
                    <div class="container">
                        <div class="banner-content">
                            <span class="category">
                                !! Siempre hay ofertas !!
                            </span>
                            <h1 class="title">Visitanos con tu familia y se testigo de la mejor experiencia de ahorro</h1>
                            <p>No esperes a que te lo cuente</p>
                            <div class="video-button-group">
                                <a href="{{route('products_index')}}" class="custom-button active">Revisa nuestros productos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-pagination"></div>
        <div class="banner-prev">
            <i class="flaticon-left-arrow"></i>
        </div>
        <div class="banner-next">
            <i class="flaticon-right-arrow"></i>
        </div>
    </section>
    <section class="choose-us-section padding-top padding-bottom">
        <div class="container">
            <div class="section-header wow fadeInUp">

                <h2 class="title">Porque escogernos</h2>
            </div>
            <div class="row mb-30-none justify-content-center">
                <div class="col-md-6 col-xl-4 wow fadeInUp" data-wow-delay=".3s">
                    <div class="choose-item choose-item-two">
                        <div class="choose-header-area">
                            <div class="choose-thumb">
                                <i class="flaticon-global"></i>
                            </div>
                            <h5 class="title">Productos importados de calidad</h5>
                        </div>
                        <div class="choose-content">
                            <p>Encuentra los mejores porductos importados al mejor precio</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 wow fadeInUp" data-wow-delay=".3s">
                    <div class="choose-item choose-item-two">
                        <div class="choose-header-area">
                            <div class="choose-thumb">
                                <i class="flaticon-clock"></i>
                            </div>
                            <h5 class="title">Rápida atención</h5>
                        </div>
                        <div class="choose-content">
                            <p>Olvídate de las largas esperas. Te atenderemos rápido</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 wow fadeInUp" data-wow-delay=".3s">
                    <div class="choose-item choose-item-two">
                        <div class="choose-header-area">
                            <div class="choose-thumb">
                                <i class="flaticon-ui"></i>
                            </div>
                            <h5 class="title">Realiza pagos de servicios básicos</h5>
                        </div>
                        <div class="choose-content">
                            <p>Aprovecha tu tiempo y pagas las planillas de tus servicios básicos</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=================Chooose Us================= -->

    <!--=================Overview Section================= -->

    <!--=================Overview Section================= -->
    <section class="overview-section bg-ash wow fadeIn">
        <div class="container-fluid p-lg-0">
            <div class="row m-0 flex-wrap-reverse justify-content-between">
                <div class="col-lg-6 padding-bottom padding-top">
                    <div class="overview-one-content mr-lg-15-xl">
                        <h2 class="title">Beneficios que obtendras</h2>
                        <p>Ahorraras una gran cantidad de dinero y disfrutarás de nuestros puestos de comida típica mientras los mas pequeños se entretienen en los espacios de recreacion pensados precisamente solo para ellos</p>
                        <ul class="bullet-list">
                            <li>
                                Serás más feliz
                            </li>
                            <li>
                                Ahorrarás como en ningún otro lugar
                            </li>
                            <li>
                                Comeras muy rico
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 p-0 col-xl-5">
                    <div class="overview-one-thumb w-100 h-100 bg_img"
                         data-background="images/banner/ba1.jpg">
                        <img class="d-lg-none" src="images/banner/ba1.jpg" alt="overview">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=================Overview Section================= -->

    <!--=================Our Service================= -->
    <section class="service-section padding-bottom padding-top">
        <div class="container">
            <div class="section-header wow fadeInUp">

                <h2 class="title">Aqui encontrarás:</h2>
            </div>
            <div class="row justify-content-center mb-30-none">
                <div class="col-md-6 col-lg-4">
                    <div class="service-item text-center wow fadeInUp" data-wow-delay=".3s">
                        <div class="service-thumb">
                            <i class="flaticon-love"></i>
                        </div>
                        <div class="service-content">
                            <h4 class="title"><a href="#0">Consiente tu Celular</a></h4>
                            <p>Contamos con locales de servicio técnico para consentir tu celular con los mejores profesionales </p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="service-item text-center wow fadeInUp" data-wow-delay=".3s">
                        <div class="service-thumb">
                            <i class="flaticon-sms"></i>
                        </div>
                        <div class="service-content">
                            <h4 class="title"><a href="#0">Los mejores dispositivos</a></h4>
                            <p>Adquiere ese celular que tanto deseas al mejor precio</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="service-item text-center wow fadeInUp" data-wow-delay=".3s">
                        <div class="service-thumb">
                            <i class="fas fa-utensils"></i>

                        </div>
                        <div class="service-content">
                            <h4 class="title"><a href="#0">la mejor comida</a></h4>
                            <p>Prueba la mejor comida tipica en nuestros restaurantes. No olvides de pedir !YAPA!</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="service-item text-center wow fadeInUp" data-wow-delay=".3s">
                        <div class="service-thumb">
                            <i class="fas fa-tshirt"></i>

                        </div>
                        <div class="service-content">
                            <h4 class="title"><a href="{{route('products_index')}}">Ponte de moda</a></h4>
                            <p>Compra la mejor ropa al mejor precio y vistete siempre a la moda</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="service-item text-center wow fadeInUp" data-wow-delay=".3s">
                        <div class="service-thumb">
                            <i class="flaticon-megaphone"></i>
                        </div>
                        <div class="service-content">
                            <h4 class="title"><a href="{{route('event')}}">Tenemos excelentes eventos</a></h4>
                            <p>Ven y diviertete en familia</p>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="service-item text-center wow fadeInUp" data-wow-delay=".3s">
                        <div class="service-thumb">
                            <i class="flaticon-paper-plane"></i>
                        </div>
                        <div class="service-content">
                            <h4 class="title"><a href="#0">No esperes más</a></h4>
                            <p>Tenemos miles de opciones para hacerte pasar momentos grandiosos</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--=================Our Service================= -->

    <!--=================Counter================= -->
    <section class="counter-up-section">
        <div class="container">
            <div class="counter-wrapper">
                <div class="counter-item wow fadeInUp" data-wow-delay=".3s">
                    <div class="counter-header">
                        <i class="flaticon-call-center"></i>
                        <h2 class="title">
                            <span class="counter">+400</span>
                        </h2>
                    </div>
                    <p>Locales</p>
                </div>
                <div class="counter-item wow fadeInUp" data-wow-delay=".3s">
                    <div class="counter-header">
                        <i class="flaticon-happiness"></i>
                        <h2 class="title">
                            <span class="counter">+350</span>k
                        </h2>
                    </div>
                    <p>Clientes felíces</p>
                </div>
                <div class="counter-item wow fadeInUp" data-wow-delay=".3s">
                    <div class="counter-header">
                        <i class="flaticon-project"></i>
                        <h2 class="title">
                            <span class="counter">+230</span>
                        </h2>
                    </div>
                    <p>Minutos ahorrados</p>
                </div>
                <div class="counter-item wow fadeInUp" data-wow-delay=".3s">
                    <div class="counter-header">
                        <i class="flaticon-collaboration"></i>
                        <h2 class="title">
                            <span class="counter">+10</span>
                        </h2>
                    </div>
                    <p>Miembros de seguridad</p>
                </div>
            </div>
        </div>
    </section>
    <!--=================Counter================= -->


@stop
