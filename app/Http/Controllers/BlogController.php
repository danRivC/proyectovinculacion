<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Post;

class BlogController extends Controller
{
    public function index()
    {

        $posts = Post::paginate(6);
        return view('blog.index', ['posts'=>$posts]);
    }
    public function show($slug)
    {
        $post=Post::where('slug', $slug)->first();
        $comments = Comment::where('post_id',$post->id)->get();

        return view('blog.detailPost', ['post'=>$post, 'comments'=>$comments]);
    }

}
