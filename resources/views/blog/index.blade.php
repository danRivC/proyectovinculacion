@extends('layouts.base')
@section('title')Blog-Chiriyacu @stop
@section('content')
    <section class="hero-area bg_img" data-background="{{URL::asset('images/page-header.jpg')}}">
        <div class="container">
            <h1 class="title m-0">Blog</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Blog
                </li>
            </ul>
        </div>
    </div>
    <section class="blog-section padding-bottom padding-top">
        <div class="container">
            <div class="row mb-30-none justify-content-center">
                @foreach($posts as $post)
                <div class="col-md-6 col-lg-4">
                    <div class="post-item">
                        <div class="post-thumb">
                            <a href="/blog/{{$post->slug}}">
                                <img src="{{asset('storage/'.$post->image)}}" alt="blog">
                            </a>
                            <div class="post-date">
                                <span>{{$post->created_at->format('d')}}</span>
                                <span>{{$post->created_at->format('M')}}</span>
                            </div>
                        </div>
                        <div class="post-content">
                            <h4 class="title">
                                <a href="/blog/{{$post->slug}}">{{$post->title}}</a>
                            </h4>
                            <p>{{$post->excerpt}}</p>
                            <a class="read-more" href="/blog/{{$post->slug}}">Leer más<i
                                    class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="blog-pagination text-center">
            {{$posts->links()}}
        </div>

        </div>
    </section>





@stop
