<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function save(Request $request)
    {

        $user = \Auth::user();
        $post_id = $request->input('post_id');
        $post_slug = $request->input('post_slug');
        $content = $request->input('content');

        $comment = new Comment();
        $comment->user_id = $user->id;
        $comment->post_id = $post_id;
        $comment->content = $content;

        $comment->save();

        return redirect()->route('blog_detail', ['slug'=>$post_slug]);
    }
}
