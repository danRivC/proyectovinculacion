<?php


namespace App\Exports;


use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class Contract implements FromView
{
    public function view(): View
    {

        $ci = $_SESSION['ci'];

        $users = DB::table("users")
            ->select('users.*')
            ->get();


//DB::select('select * from users inner join arrendatarios a on users.id = a.user_id where ci = '.$ci.'  and a.created_at between '.$date_start->format('Y-m-d').' and '.$date_end->format('Y-m-d'))

        return view('exports.arrendatario', [
            '$users' => $users
        ]);
    }
}
