<header class="style-two">
    <div class="header-top d-none d-md-block bg-theme">
        <div class="container">
            <div class="header-top-wrapper">
                <div class="row">
                    <div class="col-md-8">
                        <ul>
                            <li class="mr-3">
                                <a href="Tel:098 412 3697"><i class="fas fa-phone-square"></i>098 412 3697</a>
                            </li>
                            <li>
                                <a href="Mailto:info@centrochiriyacu.ec"><i class="fas fa-envelope"></i>info@centrochiriyacu.ec</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 d-flex flex-wrap align-items-center justify-content-end">
                        <ul class="social">
                            <li>
                                <a href="#0">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#0">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#0">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#0">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="d-none d-md-block">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="header-area">
                <div class="logo">

                </div>
                <ul class="menu">
                    <li>
                        <a href="{{route('home')}}">Inicio</a>

                    </li>
                    <li>
                        <a href="{{route('products_index')}}">Productos</a>

                    </li>
                    <li>
                        <a href="{{route('blog_index')}}">Blog</a>

                    </li>
                    <li>
                        <a href="{{route('event')}}">Eventos</a>

                    </li>
                    <li>
                        @if (Auth::guest())
                            <a href="{{route('login')}}">Iniciar Sesion</a>
                        @else
                            <a class="" href="admin/">Admin</a>
                            <a class="" href="{{route('logout')}}">Salir</a>
                        @endif
                    </li>
                </ul>
                <div class="header-bar d-lg-none">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="select-bar-bar">
                    <i class="fas fa-ellipsis-v"></i>
                </div>
                <div class="d-flex select-career justify-content-end">

                    <a href="#0" class="search-bar">
                        <i class="flaticon-magnifying-glass"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="search-form-area">
        <span class="hide-form">
            <i class="fas fa-times"></i>
        </span>
    <form class="search-form">
        <input type="text" placeholder="Search Here">
        <button type="submit"><i class="flaticon-search"></i></button>
    </form>
</div>


