@extends('voyager::master')
@section('page_title','Descargar Reportes')
@section('page_header')
    <h1 class="page-title">Imprimir Contrato de Arriendo</h1>
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body" >
                        {!! Form::open(['route'=>'voyager.contract.download','method'=>'POST']) !!}
                        <div class="row">
                            @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            @endif

                            <div class="form-group col-md-2">
                                <label for="ci">Cédula de identidad</label>
                                {!! Form::text('ci', null,['style'=>'margin-top:5px', 'class'=>'form-control', 'placeholder'=>'Cédula']) !!}
                            </div>

                            <div class="form-group col-md-2" style="margin-bottom: 300px">
                                <label for="date_end">Fecha del contrato</label>
                                {!! Form::date('date_contract', null,['style'=>'margin-top:5px', 'class'=>'form-control', 'placeholder'=>'Fecha Final']) !!}
                            </div>
                            <div class="form-group col-md-2">
                                {!! Form::submit('Generar Contrato', ['class' =>'btn btn-primary btn-block btn-md pull-right']) !!}
                            </div>


                        </div>
                        {!! Form::close() !!}

                    </div>

                </div>

            </div>

        </div>

    </div>
@stop
