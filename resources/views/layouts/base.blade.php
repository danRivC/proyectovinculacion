<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;subset=latin&amp;' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Noto+Sans%3Aregular%2Citalic%2C700%2C700italic&amp;subset=greek%2Ccyrillic-ext%2Ccyrillic%2Clatin%2Clatin-ext%2Cvietnamese%2Cgreek-ext&amp;' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Merriweather%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&amp;subset=latin%2Clatin-ext&amp;' type='text/css' media='all' />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Mystery+Quest%3Aregular&amp;subset=latin%2Clatin-ext&amp;' type='text/css' media='all' />
    @section('meta')
    @show



    {{HTML::style('css/style2.css')}}
    {{HTML::style('css/slick.css')}}
    {{HTML::style('css/slick-theme.css')}}
    {{HTML::style('css/nouislider.min.css')}}
    {{HTML::style('css/font-awesome.min.css')}}
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/all.min.css')}}
    {{HTML::style('css/animate.css')}}
    {{HTML::style('css/flaticon.css')}}
    {{HTML::style('css/lightcase.css')}}
    {{HTML::style('css/swiper.min.css')}}
    {{HTML::style('css/nice-select.css')}}
    {{HTML::style('css/main.css')}}





    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7CRaleway:400,100,200,300%7CHind:400,300" rel="stylesheet" type="text/css">

    <title>@yield('title')</title>
</head>

<body >
<div class="preloader">
    <div class="preloader-wrapper">
        <img src="{{URL::asset('css/ajax-loader.gif')}}" alt="ajax-loader">
    </div>
</div>
<a href="#0" class="scrollToTop">
    <img src="{{URL::asset('images/rocket.png')}}" alt="rocket">
</a>
<div>
@section('header')
    @include('includes.header')
@show
    @yield('content')

@section('footer')
    @include('includes.footer')
@show


</div>

<script src="{{URL::asset('js/jquery/jquery.js')}}"></script>
<script src="{{URL::asset('js/products/slick.min.js')}}"></script>
<script src="{{URL::asset('js/products/nouislider.min.js')}}"></script>


<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- Jquery -->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Datepicker Files -->
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker3.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap-standalone.css')}}">
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>hjt654
<!-- Languaje -->
<script src="{{asset('locales/bootstrap-datepicker.es.min.js')}}"></script>





<script src="{{URL::asset('js/products/jquery.zoom.min.js')}}"></script>
<script src="{{URL::asset('js/products/main.js')}}"></script>
<script>
    var ms_grabbing_curosr = 'plugins/masterslider/public/assets/css/common/grabbing.html',
        ms_grab_curosr = 'plugins/masterslider/public/assets/css/common/grab.html';
</script>
<script type='text/javascript' src='{{URL::asset('plugins/superfish/js/superfish.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('js/hoverIntent.min.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/dl-menu/modernizr.custom.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/dl-menu/jquery.dlmenu.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/jquery.easing.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/fancybox/jquery.fancybox.pack.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/fancybox/helpers/jquery.fancybox-media.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/fancybox/helpers/jquery.fancybox-thumbs.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/flexslider/jquery.flexslider.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/jquery.isotope.min.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('js/plugins.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/masterslider/public/assets/js/masterslider.min.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/jquery.transit.min.js')}}'></script>
<script type='text/javascript' src='{{URL::asset('plugins/gdlr-portfolio/gdlr-portfolio-script.js')}}'></script>
<script type="text/javascript" src="{{URL::asset('js/products/slick.min.js')}}"></script>



</body>
</html>

