<?php
namespace App\Exports;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;
class Aliquot implements FromView
{
    public function view(): View
    {
        $local = $_SESSION['local'];
        $date_start = new \DateTime($_SESSION['date_start']);
        $date_end = new \DateTime($_SESSION['date_end']) ;
        $aliquotas = DB::table('aliquots')
            ->join("shops", "shops.id","=","aliquots.local_id")
            ->join("users", "users.id", "=","shops.user_id")
            ->where("shops.number_shop","=", $local)
            ->whereBetween('aliquots.payment_date', [$date_start, $date_end])
            ->select('aliquots.*','shops.*','users.*', DB::raw('CASE WHEN aliquots.month = 1 THEN \'Enero\'
            when aliquots.month = 2 then \'Febrero\'
            when aliquots.month = 3 then \'Marzo\'
            when aliquots.month = 4 then \'Abril\'
            when aliquots.month = 5 then \'Mayo\'
            when aliquots.month = 6 then \'Junio\'
            when aliquots.month = 7 then \'Julio\'
            when aliquots.month = 8 then \'Agosto\'
            when aliquots.month = 9 then \'Septiembre\'
            when aliquots.month = 10 then \'Octubre\'
            when aliquots.month = 11 then \'Noviembre\'
            when aliquots.month = 12 then \'Diciembre\'
            END mes'))
            ->get();

        return view('exports.aliquot', [
            'aliquots' => $aliquotas
        ]);
    }

}
