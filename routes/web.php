<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Post;

Route::get('/', function () {

    $titulo = "Centro Comercial Chiriyacu";
    return view('home')
        ->with('titulo', $titulo);
});


Auth::routes();

Route::get('/blog', 'BlogController@index')->name('blog_index');
Route::get('/blog/{slug}', 'BlogController@show')->name('blog_detail');
Route::post('/comment/save','CommentController@save')->name('comment.save');
Route::get('/event', 'EventController@index')->name('event');
Route::get('/event/{slug}', 'EventController@show')->name('event_detail');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/products','ProductsController@index')->name('products_index');
Route::get('/products/{slug}', 'ProductsController@searchShop')->name('product_shop');

Route::get('/shops/{id}', 'ShopController@index')->name('shop_index');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::post('/exports/download', 'ExporterController@export')->name('voyager.exports.download');
    Route::post('/report-aliquot/download', 'ReportAliquotUser@export')->name('voyager.aliquot.download');
    Route::post('/report-lease/download', 'ReportLeaseUser@export')->name('voyager.lease.download');
    Route::post('/contract-lease/download', 'ContractLeasesController@export')->name('voyager.contract.download');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
