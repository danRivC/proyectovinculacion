<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    public function index($id){
        $shop = Shop::where('id', $id)->first();
        return view('shop/index', ['shop'=>$shop]);
    }
}
