@extends('layouts.base')
@section('title')Eventos @stop


@section('content')
    <section class="hero-area bg_img" data-background="images/page-header.jpg">
        <div class="container">
            <h1 class="title m-0">Próximos Eventos</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Eventos
                </li>
            </ul>
        </div>
    </div>
    <section class="blog-section padding-bottom padding-top">
        <div class="container">
            <div class="row mb-30-none justify-content-center">
                @foreach($events as $event)
                    <div class="col-md-6 col-lg-4">
                        <div class="post-item">
                            <div class="post-thumb">
                                <a href="/event/{{$event->slug}}">
                                    <img src="{{asset('storage/'.$event->image)}}" alt="blog">
                                </a>
                                <div class="post-date">
                                    <span>{{$event->created_at->format('d')}}</span>
                                    <span>{{$event->created_at->format('M')}}</span>
                                </div>
                            </div>
                            <div class="post-content">
                                <h4 class="title">
                                    <a href="/event/{{$event->slug}}">{{$event->title}}</a>
                                </h4>
                                <p>{{$event->preview}}</p>
                                <a class="read-more" href="/event/{{$event->slug}}">Leer más<i
                                        class="fas fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="blog-pagination text-center">
            {{$events->links()}}
        </div>

        </div>
    </section>

@stop
