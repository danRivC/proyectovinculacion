@extends('layouts.base')

@section('content')
    <section class="hero-area bg_img" data-background="images/page-header.jpg">
        <div class="container">
            <h1 class="title m-0">Iniciar Sesión</h1>
        </div>
    </section>
    <div class="breadcrumb-section">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Iniciar Sesión
                </li>
            </ul>
        </div>
    </div>
    <section class="account-section padding-top padding-bottom bg_img bg_right_bottom"
             data-background="images/account/account-bg.png">
        <div class="container">
            <div class="account-wrapper">
                <div class="account-area">
                    <h3 class="account-title">Ingresa a tu cuenta</h3>
                    <form class="sign-in-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus  class="form-control @error('email') is-invalid @enderror" placeholder="Ingresa tu Email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Contraseña" id="my-input-01" class=" form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <span class="view-pass" id="view-pass-01">
                                <i class="flaticon-eye"></i>
                            </span>

                        </div>
                        <div class="col-lg-12">
                            <div class="row">

                                <div class="col-lg-3" style="padding-right: 0">
                                    <label  for="remember">
                                        Recordar ?
                                    </label>
                                </div>
                                <div class="col-lg-1" style="padding-left: 0">
                                    <input type="checkbox" style="width: 15px; height: 15px;" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Ingresar">
                        </div>
                    </form>
                    <div class="forget-pass-group d-flex flex-wrap justify-content-between">

                        <div class="no-account">
                            <p>No tienes cuenta? <a href="{{url('register')}}">Registrate</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
