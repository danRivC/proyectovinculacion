<footer>
    <div class="footer-top padding-top padding-bottom">
        <div class="container">
            <div class="row mb-50-none">
                <div class="col-sm-6 col-lg-3">
                    <div class="footer-widget footer-link">
                        <h5 class="title">bulk SMS</h5>
                        <ul>
                            <li>
                                <a href="#0">masking SMS</a>
                            </li>
                            <li>
                                <a href="#0">Non-Masking SMS</a>
                            </li>
                            <li>
                                <a href="#0">location based SMS</a>
                            </li>
                            <li>
                                <a href="#0">Voice message</a>
                            </li>
                            <li>
                                <a href="#0">promo SMS</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="footer-widget footer-link">
                        <h5 class="title">company</h5>
                        <ul>
                            <li>
                                <a href="#0">about</a>
                            </li>
                            <li>
                                <a href="#0">pricing plan</a>
                            </li>
                            <li>
                                <a href="#0">faq</a>
                            </li>
                            <li>
                                <a href="#0">testiminial</a>
                            </li>
                            <li>
                                <a href="#0">promotion</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="footer-widget footer-link">
                        <h5 class="title">Partners</h5>
                        <ul>
                            <li>
                                <a href="#0">kingstar</a>
                            </li>
                            <li>
                                <a href="#0">laravala click</a>
                            </li>
                            <li>
                                <a href="#0">yago</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="footer-widget footer-about">
                        <h5 class="title">about us</h5>
                        <p>Tellus fermentum a aenean laoreet libero in, at convallis varius morbi.</p>
                        <ul class="footer-social">
                            <li>
                                <a href="#0"><i class="fab fa-facebook-f"></i></a>
                            </li>
                            <li>
                                <a href="#0"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#0"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#0"><i class="fab fa-linkedin-in"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom py-3 py-sm-4 text-center">
        <div class="container">
            <p class="m-0"> Centro comercial Chiriyacu - Desarrollado por <a href="https://www.itecsur.edu.ec/">Instituto Superior CompuSur</a> </p>
        </div>
    </div>
</footer>
<!--=================Footer Section================= -->

<script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ URL::asset('js/modernizr-3.6.0.min.js') }}"></script>
<script src="{{ URL::asset('js/plugins.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/waypoint.js') }}"></script>
<script src="{{ URL::asset('js/isotope.pkgd.min.js') }}"></script>
<script src="{{ URL::asset('js/lightcase.js') }}"></script>
<script src="{{ URL::asset('js/swiper.min.js') }}"></script>
<script src="{{ URL::asset('js/wow.min.js') }}"></script>
<script src="{{ URL::asset('js/countdown.min.js') }}"></script>
<script src="{{ URL::asset('js/counterup.min.js') }}"></script>
<script src="{{ URL::asset('js/nice-select.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
