<?php
namespace App\Exports;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class Arrendatario implements FromView
{
    public function view(): View
    {
        $date_start = new \DateTime($_SESSION['date_start']);
        $local = $_SESSION['local'];
        $date_end = new \DateTime($_SESSION['date_end']) ;
        $arrendatarios = DB::table("leases")
            ->join("shops", "shops.id","=","leases.local_id")
            ->join("users", "users.id", "=","shops.user_id")
            ->where("shops.number_shop","=", $local)
            ->whereBetween('leases.payment_date', [$date_start, $date_end])
            ->select('leases.*','shops.*','users.*', DB::raw('CASE WHEN leases.month = 1 THEN \'Enero\'
            when leases.month = 2 then \'Febrero\'
            when leases.month = 3 then \'Marzo\'
            when leases.month = 4 then \'Abril\'
            when leases.month = 5 then \'Mayo\'
            when leases.month = 6 then \'Junio\'
            when leases.month = 7 then \'Julio\'
            when leases.month = 8 then \'Agosto\'
            when leases.month = 9 then \'Septiembre\'
            when leases.month = 10 then \'Octubre\'
            when leases.month = 11 then \'Noviembre\'
            when leases.month = 12 then \'Diciembre\'
            END mes'))
            ->get();


//DB::select('select * from users inner join arrendatarios a on users.id = a.user_id where ci = '.$ci.'  and a.created_at between '.$date_start->format('Y-m-d').' and '.$date_end->format('Y-m-d'))

        return view('exports.arrendatario', [
            'arrendatarios' => $arrendatarios
        ]);
    }
}
