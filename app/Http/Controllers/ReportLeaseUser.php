<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ReportLeaseUser extends Controller
{
    public function __construct()
    {
        set_time_limit(0);
        ini_set('max_execution_time',3000);
        ini_set('memory_limit','256M');
    }
    public function index(){
        return view('report_lease.form');
    }
    public function export(){
        session_start();
        $_SESSION["local"] = \request('local');
        $_SESSION["date_start"] = \request('date_start');
        $_SESSION["date_end"] = \request('date_end');
        $name_document = "reporte-arriendo";
        $model = $name_document;
        $exportable = "App\\Exports\\Arrendatario";
        return Excel::download(new $exportable, "{$model}.xlsx");

    }


}
