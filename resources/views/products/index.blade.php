@extends('layouts.base')
@section('title')Productos @stop
@section('content')
    <!-- SECTION -->

    <section class="hero-area bg_img" data-background="images/page-header.jpg">
        <div class="container">
            <h1 class="title m-0">Productos</h1>
        </div>
    </section>
    <div class="breadcrumb-section" style="margin-bottom: 10px">
        <div class="container">
            <ul class="breadcrumb">
                <li>
                    <a href="{{url('home')}}">Inicio</a>
                </li>
                <li>
                    Productos
                </li>
            </ul>
        </div>
    </div>
    @foreach($types as $type)
        <div class="sponsor-section padding-bottom">
            <div class="container">
                <h3>{{$type->name}}</h3>
                <div class="sponsor-slider">
                    <div class="swiper-wrapper mt-lg-5">
                        @foreach($type->products as $product)
                            <div class="sponsor-thumb">
                                <a href="products/{{$product->slug}}"><img src="{{asset('storage/'.$product->image)}}" alt="sponsor"></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@stop
