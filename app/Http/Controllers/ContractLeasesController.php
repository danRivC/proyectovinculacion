<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContractLeasesController extends Controller
{
    public function __construct()
    {
        set_time_limit(0);
        ini_set('max_execution_time',3000);
        ini_set('memory_limit','256M');
    }
    public function index(){
        return view('contract_lease.form');
    }

    public function export(){
        $ci = \request('ci');
        $date_contract = \request('date_contract');
        $exporter = "App\\Exports\\Contract";

        $users = DB::table("users")
            ->select('users.*')
            ->where('users.ci', '=', $ci)
            ->get();


        $pdf = PDF::loadView('contract_lease.contract', compact('users' ), compact('date_contract'));
        return $pdf->download('ejemplo.pdf');

    }

}
